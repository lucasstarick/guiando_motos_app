import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';

import ViajanteReducer from './features/viajante/reducer';

const rootReducer = combineReducers({
  form: formReducer,
  viajante: ViajanteReducer,
});

export default rootReducer;
