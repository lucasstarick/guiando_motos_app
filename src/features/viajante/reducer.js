import {
  PERMISSION_GRANTED_GPS,
  SELECT_PLACES,
  PLACES_DETAILS,
  TRACE_ROUTE,
  AUTHENTICATE,
  CREATE_TRAVEL,
  TRACKER_REGISTER,
  FINISH_TRAVEL,
} from './constants';

const INITIAL_STATE = {
  gpsState: null,
  permissionLocation: false,
  origin: null,
  region: null,
  viewport: null,
  city: '',
  placesSearch: [
    {
      type: 'search',
      text: 'posto de combustível',
      search: 'posto de combustível',
    },
    {
      type: 'search',
      text: 'oficina mecânica',
      search: 'oficina moto',
    },
    {
      type: 'search',
      text: 'moto peças',
      search: 'moto peças',
    },
    {
      type: 'search',
      text: 'borracharia',
      search: 'borracharia',
    },
    {
      type: 'search',
      text: 'estacionamento',
      search: 'estacionamento',
    },
    {
      type: 'search',
      text: 'lava jato',
      search: 'lava jato',
    },
  ],
  placesSelected: [],
  place: {
    name: '',
    formatted_address: '',
    formatted_phone_number: '',
    photos: [],
    geometry: {
      location: null,
      viewport: null,
    },
  },
  login: {
    uid: null,
    isAnonymous: false,
  },
  showDetails: false,
  traceRoute: false,
  loading: true,
  travelFinish: false,
  travel: {
    id: null,
    status: false,
    location: null,
    speed: null,
    waypoints: [],
  },
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case PERMISSION_GRANTED_GPS:
      console.log({payload: action.payload});
      return {
        ...state,
        ...action.payload,
        loading: false,
      };
    case SELECT_PLACES:
      return {
        ...state,
        ...action.payload,
      };
    case PLACES_DETAILS:
      return {
        ...state,
        ...action.payload,
      };
    case TRACE_ROUTE:
      return {
        ...state,
        ...action.payload,
      };
    case CREATE_TRAVEL:
      return {
        ...state,
        travel: {
          ...state.travel,
          id: action.payload.travelId,
          location: state.origin,
          status: true,
        },
      };
    case TRACKER_REGISTER:
      return {
        ...state,
        travel: {
          ...state.travel,
          ...action.payload,
        },
      };
    case FINISH_TRAVEL:
      return {
        ...state,
        traceRoute: false,
        placesSelected: [],
        travelFinish: true,
        travel: {
          status: false,
          ...state.travel,
          ...action.payload,
        },
      };
    case AUTHENTICATE:
      return {
        ...state,
        user: {
          ...state.user,
          ...action.payload,
        },
      };
    default:
      return state;
  }
};
