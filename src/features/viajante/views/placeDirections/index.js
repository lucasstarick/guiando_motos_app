import React from 'react';
import MapViewDirections from 'react-native-maps-directions';

const PlaceDirections = ({destination, origin, waypoints, onReady}) => {
  return (
    <MapViewDirections
      destination={destination}
      origin={origin}
      onReady={onReady}
      waypoints={waypoints}
      apikey={'AIzaSyBXNwqLXWMzjpOo83hGDPi_rnkLVmBy0WA'}
      strokeWidth={3}
      strokeColor="#222"
    />
  );
};

export default PlaceDirections;
