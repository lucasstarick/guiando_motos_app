import React, {Component} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {Dimensions, Linking} from 'react-native';
import Animated, {Easing} from 'react-native-reanimated';
import Foundation from 'react-native-vector-icons/Foundation';
import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';

import {placeDetails, traceRoute} from '../../navegarNoMapa';

import {
  Container,
  Gallery,
  Img,
  Estabilishment,
  Name,
  Address,
  Options,
  Directions,
  Phone,
} from './styles';

const {height} = Dimensions.get('window');

class PlaceDetails extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animatedTopValue: new Animated.Value(height),
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.showDetails !== prevProps.showDetails) {
      this.openDetailsModal();
    }
  }

  openDetailsModal() {
    const {showDetails} = this.props;

    Animated.timing(this.state.animatedTopValue, {
      toValue: showDetails ? height / 2 : height,
      duration: 250,
      easing: Easing.linear(Easing.linear),
    }).start();
  }

  render() {
    const {animatedTopValue} = this.state;
    const {
      name,
      formatted_address,
      formatted_phone_number,
      photos,
    } = this.props.place;

    return (
      <Container
        style={{
          top: animatedTopValue,
        }}>
        <Gallery>
          {photos.length > 0 ? (
            photos.map((photo, index) => {
              return (
                <Img
                  key={index}
                  source={{
                    uri: `${photo.url}`,
                  }}
                />
              );
            })
          ) : (
            <Img
              source={require('../../../../assets/img/no_image.png')}
              resizeMode="contain"
            />
          )}
        </Gallery>
        <Estabilishment>
          <Name>{name}</Name>
          <Address>{formatted_address}</Address>
        </Estabilishment>
        <Options>
          <Directions onPress={() => this.props.traceRoute()}>
            <FontAwesome5 name="directions" size={30} color="#fff" />
          </Directions>
          <Phone
            onPress={() => Linking.openURL(`tel:${formatted_phone_number}`)}>
            <Foundation name="telephone" size={30} color="#fff" />
          </Phone>
        </Options>
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  showDetails: state.viajante.showDetails,
  place: state.viajante.place,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      placeDetails,
      traceRoute,
    },
    dispatch,
  );

PlaceDetails = connect(mapStateToProps, mapDispatchToProps)(PlaceDetails);

export default PlaceDetails;
