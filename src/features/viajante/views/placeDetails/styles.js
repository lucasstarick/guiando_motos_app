import styled from 'styled-components/native';
import Animated from 'react-native-reanimated';

export const Container = styled(Animated.View)`
  background: #fff;
  position: absolute;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
  height: 50%;
  padding: 0 10px;
  z-index: 999;
`;

export const Gallery = styled.ScrollView.attrs(() => ({
  horizontal: true,
  showsHorizontalScrollIndicator: false,
  contentContainerStyle: {
    alignItems: 'center',
    justifyContent: 'center',
    minWidth: '100%',
  },
}))``;

export const Img = styled.Image`
  width: 200px;
  height: 135px;
  margin-right: 10px;
`;

export const Estabilishment = styled.View`
  margin-top: 10px;
`;
export const Name = styled.Text`
  font-size: 16px;
  font-weight: bold;
`;
export const Address = styled.Text``;
export const Options = styled.View`
  margin: 20px 0px;
  flex-direction: row;
  justify-content: space-between;
`;
export const Directions = styled.TouchableOpacity`
  width: 30%;
  margin-left: 10px;
  padding: 5px;
  border-radius: 20px;
  align-items: center;
  background: #258fdb;
`;
export const Phone = styled.TouchableOpacity`
  width: 30%;
  padding: 5px;
  border-radius: 20px;
  align-items: center;
  background: #038f26;
  margin-right: 10px;
`;
