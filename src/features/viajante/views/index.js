import React, {Component, Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {
  StyleSheet,
  Linking,
  BackHandler,
  Button,
  Text,
  Alert,
  Platform,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import RNExitApp from 'react-native-exit-app';
import GPSState from 'react-native-gps-state';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';

import SearchPlace from './searchPlace';
import PlaceDirections from './placeDirections';

import {getPixelSize} from '../../../utils';
import {
  authenticate,
  requestLocation,
  placeDetails,
  dismissTraceRoute,
  dismissPlaceDetails,
  createTravel,
  trackerTravel,
  finishTravel,
  dismissTravel,
} from '../navegarNoMapa';

import {Container} from './styles';
import PlaceDetails from './placeDetails';

import markerImage from '../../../assets/img/motorcycle-icon.png';

class Viajante extends Component {
  constructor(props) {
    super(props);

    this.state = {
      trackerCoordinates: null,
      locations: [],
    };
  }

  componentDidMount() {
    //Permissão de acesso e posição atual
    GPSState.addListener((status) => {
      this.props.requestLocation(status);
    });
    GPSState.requestAuthorization(GPSState.AUTHORIZED_ALWAYS);

    //Requisição de login Anônimo ou não
    this.props.authenticate();

    BackgroundGeolocation.configure({
      stationaryRadius: 50,
      distanceFilter: 200,
      interval: 8000,
      fastestInterval: 8000,
      activitiesInterval: 8000,
    });

    BackgroundGeolocation.on('start', () => {
      console.log('[DEBUG] BackgroundGeolocation has been started');
      this.setState({isRunning: true});
    });

    BackgroundGeolocation.on('stop', () => {
      console.log('[DEBUG] BackgroundGeolocation has been stopped');
      this.setState({isRunning: false});
    });

    BackgroundGeolocation.on('location', (location) => {
      console.log('[DEBUG] BackgroundGeolocation location', location);
      const {latitude, longitude, speed, time} = location;
      this.props.trackerTravel({
        location: {
          latitude,
          longitude,
          latitudeDelta: 0.0134,
          longitudeDelta: 0.0134,
        },
        speed,
        time,
        travelId: this.props.viajante.travel.id,
      });

      this.setState({
        locations: [
          ...this.state.locations,
          {latitude: location.latitude, longitude: location.longitude},
        ],
      });

      BackgroundGeolocation.startTask((taskKey) => {
        requestAnimationFrame(() => {
          // const latitudeDelta = 0.0143;
          // const longitudeDelta = 0.0134;
          // const region = Object.assign({}, location, {
          //   latitudeDelta,
          //   longitudeDelta,
          // });
          // const locations = this.state.locations.slice(0);
          // locations.push(location);
          BackgroundGeolocation.endTask(taskKey);
        });
      });
    });

    BackgroundGeolocation.on('foreground', () => {
      console.log('[INFO] App is in foreground');

      if (this.props.viajante.travel.status) {
        this.mapView.fitToCoordinates(this.state.trackerCoordinates, {
          edgePadding: {
            right: getPixelSize(50),
            left: getPixelSize(50),
            top: getPixelSize(50),
            bottom: getPixelSize(50),
          },
        });
      }
    });

    BackgroundGeolocation.on('background', () => {
      console.log('[INFO] App is in background');
    });

    //Controle do Botão de Voltar
    BackHandler.addEventListener('hardwareBackPress', this.backAction);
  }

  componentDidUpdate(prevProps, prevState) {
    if (
      JSON.stringify(this.props.viajante.placesSelected) !==
        JSON.stringify(prevProps.viajante.placesSelected) &&
      this.props.viajante.permissionLocation
    ) {
      this.mapView.fitToElements(true);
    }
  }

  componentWillUnmount() {
    BackgroundGeolocation.removeAllListeners();

    GPSState.removeListener();
  }

  toggleTracking(place) {
    BackgroundGeolocation.checkStatus(
      ({isRunning, locationServicesEnabled, authorization}) => {
        if (isRunning) {
          this.props.finishTravel(this.props.viajante.travel.id);

          return false;
        }

        this.props.createTravel(place, this.props.viajante.origin);

        Alert.alert(
          'Direcionamento',
          'Deseja obter informações de direcionamento?',
          [
            {
              text: 'Sim',
              onPress: () => this.openGps(place),
            },
            {
              text: 'Não',
              onPress: () => {},
            },
          ],
        );
      },
    );
  }

  openGps(place) {
    const {lat, lng} = place.geometry.location;

    const location = `${lat},${lng}`;
    const url = Platform.select({
      ios: `maps:${location}`,
      android: `geo:${location}?center=${location}&q=${location}&z=16`,
    });

    Linking.openURL(url);
  }

  async onMarkerPress(mapEventData) {
    const markerID = mapEventData._targetInst.return.key;
    this.props.placeDetails(this.props.viajante.placesSelected[markerID]);
  }

  backAction = () => {
    const {traceRoute, showDetails, travelFinish} = this.props.viajante;

    if (traceRoute) {
      this.props.dismissTraceRoute();
    } else if (showDetails) {
      this.props.dismissPlaceDetails();
      this.mapView.fitToElements(true);
    } else if (travelFinish) {
      console.log('back travelFinish');
      this.props.dismissTravel();
    } else {
      RNExitApp.exitApp();
    }

    return true;
  };

  render() {
    const {
      origin,
      region,
      placesSelected,
      place,
      traceRoute,
      travelFinish,
      permissionLocation,
      travel,
    } = this.props.viajante;

    const {loading} = this.props;

    if (loading) {
      return null;
    }

    return (
      <Container>
        {!permissionLocation && (
          <Fragment>
            <Text>Para uma melhor experiência permita o acesso ao GPS</Text>
            <Button
              title="Configurações"
              onPress={() => {
                // if (gpsState === 'RESTRICTED') {
                //   GPSState.openLocationSettings();
                // } else {
                //   Linking.openSettings();
                //   RNExitApp.exitApp();
                // }
                BackgroundGeolocation.showLocationSettings();
              }}
            />
          </Fragment>
        )}

        {permissionLocation && <SearchPlace />}

        {permissionLocation && <PlaceDetails />}

        <MapView
          provider={PROVIDER_GOOGLE}
          style={styles.map}
          region={region}
          ref={(el) => (this.mapView = el)}
          showsUserLocation={!traceRoute && !travelFinish}
          moveOnMarkerPress={false}
          loadingEnabled>
          {!traceRoute &&
            placesSelected.map((item, index) => {
              return (
                <Marker
                  key={index}
                  coordinate={{
                    latitude: item.geometry.location.lat,
                    longitude: item.geometry.location.lng,
                  }}
                  onPress={(e) => this.onMarkerPress(e)}
                />
              );
            })}

          {traceRoute && !travelFinish && (
            <Fragment>
              <PlaceDirections
                origin={!travel.status ? origin : travel.location}
                destination={{
                  latitude: place.geometry.location.lat,
                  longitude: place.geometry.location.lng,
                }}
                onReady={(result) => {
                  // console.log(result);

                  this.setState(
                    {trackerCoordinates: result.coordinates},
                    () => {
                      this.mapView.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                          right: getPixelSize(50),
                          left: getPixelSize(50),
                          top: getPixelSize(50),
                          bottom: getPixelSize(50),
                        },
                      });
                    },
                  );
                }}
              />
              <Marker
                coordinate={!travel.status ? origin : travel.location}
                anchor={{x: 0.4, y: 1}}
                image={markerImage}
              />
              <Marker
                coordinate={{
                  latitude: place.geometry.location.lat,
                  longitude: place.geometry.location.lng,
                }}
              />
            </Fragment>
          )}

          {travelFinish && (
            <Fragment>
              <PlaceDirections
                origin={travel.origin}
                destination={travel.location}
                waypoints={travel.waypoints}
                onReady={(result) => {
                  console.log({resultTravelFinish: result});

                  this.setState(
                    {trackerCoordinates: result.coordinates},
                    () => {
                      this.mapView.fitToCoordinates(result.coordinates, {
                        edgePadding: {
                          right: getPixelSize(50),
                          left: getPixelSize(50),
                          top: getPixelSize(50),
                          bottom: getPixelSize(50),
                        },
                      });
                    },
                  );
                }}
              />
              <Marker coordinate={travel.origin} />
              <Marker
                coordinate={travel.location}
                anchor={{x: 0.4, y: 1}}
                image={markerImage}
              />
            </Fragment>
          )}
          {/* {travel.waypoints.map((item, index) => {
            return (
              <Marker
                key={index}
                coordinate={{
                  latitude: item.latitude,
                  longitude: item.longitude,
                }}
              />
            );
          })} */}
        </MapView>

        {traceRoute && (
          <Button
            title={!travel.status ? 'Iniciar' : 'Finalizar'}
            onPress={() => {
              this.toggleTracking(place);
            }}
          />
        )}
      </Container>
    );
  }
}

const mapStateToProps = (state) => ({
  viajante: state.viajante,
  loading: state.viajante.loading,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      authenticate,
      requestLocation,
      placeDetails,
      dismissTraceRoute,
      dismissPlaceDetails,
      createTravel,
      trackerTravel,
      finishTravel,
      dismissTravel,
    },
    dispatch,
  );

Viajante = connect(mapStateToProps, mapDispatchToProps)(Viajante);

export default Viajante;

const styles = StyleSheet.create({
  map: {
    flex: 1,
    zIndex: -99999,
  },
});
