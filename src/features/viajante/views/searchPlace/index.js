import React, {Component, Fragment} from 'react';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {View, BackHandler, Dimensions} from 'react-native';
import _ from 'lodash';
import Animated, {Easing} from 'react-native-reanimated';
import Ionicons from 'react-native-vector-icons/Ionicons';
import AntDesign from 'react-native-vector-icons/AntDesign';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import {placesSelected, search} from '../../navegarNoMapa';

import {
  Search,
  SearchButton,
  SearchHeader,
  BackButton,
  SearchList,
  SearchListItem,
  SearchListIcon,
  SearchListText,
} from './styles';

const {height} = Dimensions.get('window');

class SearchPlace extends Component {
  constructor(props) {
    super(props);

    this.state = {
      animatedTopValue: new Animated.Value(height),
      showSearch: false,
    };
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.state.showSearch !== prevState.showSearch) {
      this.openSearchModal();
    }
  }

  openSearchModal() {
    const {showSearch, animatedTopValue} = this.state;

    Animated.timing(animatedTopValue, {
      toValue: showSearch ? 0 : height,
      duration: 250,
      easing: Easing.linear(Easing.linear),
    }).start(() => {
      if (showSearch) {
        BackHandler.addEventListener('hardwareBackPress', this.backAction);
      } else {
        BackHandler.removeEventListener('hardwareBackPress', this.backAction);
      }
    });
  }

  backAction = () => {
    this.setState({showSearch: false});

    return true;
  };

  render() {
    const {
      animatedTopValue,
      showSearch,
    } = this.state;

    const {placesSearch, search, city} = this.props;

    return (
      <Fragment>
        <SearchButton
          onPress={() => {
            this.setState({showSearch: !showSearch});
          }}>
          <Ionicons name="search" size={30} color="#000" />
        </SearchButton>
        <Search
          style={{
            top: animatedTopValue,
          }}>
          <SearchList>
            {placesSearch.map((place, index) => {
                return (
                  <SearchListItem
                    key={index}
                    onPress={() => {
                      this.setState({showSearch: !showSearch}, () => {
                        search(place.text, this.props.origin, city);
                      });
                    }}>
                    <SearchListIcon
                      component={
                        <Ionicons name="search" size={15} color="#ff0000" />
                      }
                      background="#f7abab"
                    />
                    <SearchListText>{place.text}</SearchListText>
                  </SearchListItem>
                );
              })}
          </SearchList>
        </Search>
      </Fragment>
    );
  }
}

const mapStateToProps = (state) => ({
  placesSearch: state.viajante.placesSearch,
  city: state.viajante.city,
  origin: state.viajante.origin,
});

const mapDispatchToProps = (dispatch) =>
  bindActionCreators(
    {
      search,
      placesSelected,
    },
    dispatch,
  );

SearchPlace = connect(mapStateToProps, mapDispatchToProps)(SearchPlace);

export default SearchPlace;
