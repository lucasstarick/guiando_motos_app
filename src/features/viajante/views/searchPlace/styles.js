import React from 'react';
import styled from 'styled-components/native';
import Animated from 'react-native-reanimated';

export const Search = styled(Animated.View)`
  background: #fff;
  position: absolute;
  padding: 10px;
  height: 100%;
  width: 100%;
  z-index: 999;
`;

export const SearchButton = styled.TouchableOpacity`
  position: absolute;
  top: 5%;
  right: 5%;
  background: #2289d6;
  border-radius: 20px;
  padding: 5px;
`;

export const SearchHeader = styled.View`
  width: 100%;
  flex-direction: row;
  padding: 5px;
  border-radius: 10px;
  border: 1px solid rgba(0, 0, 0, 0.2);
  justify-content: flex-start;
  align-items: center;
`;

export const BackButton = styled.TouchableOpacity``;

export const CloseButton = styled.TouchableOpacity``;

export const SearchInputText = styled.TextInput`
  width: 80%;
`;

export const SearchList = styled.View`
  margin-top: 5px;
`;

export const SearchListItem = styled.TouchableOpacity`
  flex-direction: row;
  border-bottom-width: 1px;
  border-bottom-color: rgba(0, 0, 0, 0.2);
  width: 100%;
  padding: 10px;
  align-items: center;
`;

export const SearchListIcon = styled(({component, ...props}) =>
  React.cloneElement(component, props),
)`
  background: ${(props) => props.background};
  padding: 8px;
  border-radius: 20px;
`;

export const SearchListText = styled.Text`
  font-weight: bold;
  margin-left: 15px;
`;

export const SearchListPlace = styled.Text`
  font-weight: bold;
  margin-left: 15px;
`;
export const SearchListAddressPlace = styled.Text`
  margin-left: 15px;
`;
