import RNGooglePlaces from 'react-native-google-places';
import Axios from '../../helpers/Axios';
import GPSState from 'react-native-gps-state';
import BackgroundGeolocation from '@mauron85/react-native-background-geolocation';

var GeoPoint = require('geopoint');

import {
  PERMISSION_GRANTED_GPS,
  SELECT_PLACES,
  PLACES_DETAILS,
  TRACE_ROUTE,
  AUTHENTICATE,
  CREATE_TRAVEL,
  TRACKER_REGISTER,
  FINISH_TRAVEL,
  RAIO,
} from './constants';

export function authenticate() {
  return (dispatch) => {
    Axios.get('/auth')
      .then((response) => {
        dispatch({
          type: AUTHENTICATE,
          payload: {
            ...response.data,
          },
        });
      })
      .catch((error) => console.log({error}));
  };
}

export function requestLocation(status) {
  return async (dispatch) => {
    let gpsState;
    switch (status) {
      case GPSState.NOT_DETERMINED:
        gpsState = 'NOT_DETERMINED';
        break;
      case GPSState.RESTRICTED:
        gpsState = 'RESTRICTED';
        break;
      case GPSState.DENIED:
        gpsState = 'DENIED';
        break;
      case 3: //AUTHORIZED (ANDROID) AUTHORIZED_ALWAYS(IOS)
        gpsState = 'AUTHORIZED_ALWAYS';
        break;
      case GPSState.AUTHORIZED_WHENINUSE:
        gpsState = 'AUTHORIZED_WHENINUSE';
        break;
    }

    if (status === 3 || status === 4) {
      let currentPlace = await getCurrentPlace();

      dispatch({
        type: PERMISSION_GRANTED_GPS,
        payload: {
          gpsState,
          permissionLocation: true,
          origin: currentPlace.region,
          region: currentPlace.region,
          viewport: currentPlace.viewport,
          city: currentPlace.city,
        },
      });
    } else {
      dispatch({
        type: PERMISSION_GRANTED_GPS,
        payload: {
          gpsState,
          permissionLocation: false,
          region: null,
          viewport: null,
        },
      });
    }
  };
}

export function createTravel(place, origin) {
  return (dispatch) => {
    let travel = {
      origin,
      establishment: place.name,
      address: place.formatted_address,
      location: {
        latitude: place.geometry.location.lat,
        longitude: place.geometry.location.lng,
      },
    };

    Axios.post('/travel', travel)
      .then((response) => {
        BackgroundGeolocation.start();

        dispatch({
          type: CREATE_TRAVEL,
          payload: {
            ...response.data,
          },
        });
      })
      .catch((error) => console.log({error}));
  };
}

export function finishTravel(travelId) {
  return (dispatch) => {
    Axios.get(`/travel/${travelId}`)
      .then((response) => {
        BackgroundGeolocation.stop();

        dispatch({
          type: FINISH_TRAVEL,
          payload: response.data.travel,
        });
      })
      .catch((error) => console.log({error}));
  };
}

export function trackerTravel(tracker) {
  return (dispatch) => {
    Axios.post('/trackRoute', tracker)
      .then((response) => {
        dispatch({
          type: TRACKER_REGISTER,
          payload: {
            location: tracker.location,
            speed: tracker.speed,
          },
        });
      })
      .catch((error) => console.log({error}));
  };
}

async function getCurrentPlace() {
  return RNGooglePlaces.getCurrentPlace(['location', 'address'])
    .then((results) => {
      //Av. Pres. Tancredo Neves, 3105 - Paquetá, Belo Horizonte - MG, 31330-430, Brazil
      let city = results[0].address
        .split(',', 3)
        .join()
        .split(',')
        .pop()
        .split('-')[0];

      let region = {
        latitude: results[0].location.latitude,
        longitude: results[0].location.longitude,
        latitudeDelta: 0.0143,
        longitudeDelta: 0.0134,
      };

      let myLocation = new GeoPoint(region.latitude, region.longitude);
      let coordinates = myLocation.boundingCoordinates(1, null, true);

      let viewport = {
        latitudeSW: coordinates[0]._degLat,
        longitudeSW: coordinates[0]._degLon,
        latitudeNE: coordinates[1]._degLat,
        longitudeNE: coordinates[1]._degLon,
      };

      return {
        region,
        viewport,
        city,
      };
    })
    .catch((error) => {
      return {
        region: null,
        viewport: null,
      };
    });
}

export function search(placeSearch, origin, city) {
  let myLocation = new GeoPoint(origin.latitude, origin.longitude);
  return async (dispatch) => {
    dispatch({
      type: SELECT_PLACES,
      payload: {
        placesSelected: [],
      },
    });

    let url =
      'https://maps.googleapis.com/maps/api/place/textsearch/json?' +
      'query=' +
      placeSearch +
      '&location=' +
      origin.latitude +
      ',' +
      origin.longitude +
      '&radius=' +
      RAIO +
      '&key=AIzaSyBXNwqLXWMzjpOo83hGDPi_rnkLVmBy0WA';

    //Realizando request a API da Google Place Autocomplete
    let response = await fetch(url);

    let json = await response.json();

    //Extraindo informações desejadas
    let promises = Promise.all(
      json.results.map(async (place) => {
        const {location} = place.geometry;

        let placeLocation = new GeoPoint(location.lat, location.lng);
        let distance = myLocation.distanceTo(placeLocation, true) * 1000; //true -> distance in km convert km -> metros

        //Retirar os fora do raio
        if (distance > RAIO) {
          return null;
        }

        //Realizando request a API da Google Place Details
        let response = await fetch(
          'https://maps.googleapis.com/maps/api/place/details/json?place_id=' +
            place.place_id +
            '&fields=place_id,name,formatted_phone_number,formatted_address,geometry,photos&sessiontoken=123456789&key=AIzaSyBXNwqLXWMzjpOo83hGDPi_rnkLVmBy0WA',
        );

        let json = await response.json();

        return json.result;
      }),
    );

    promises
      .then((result) => {
        //Excluir lugares fora da cidade
        for (var i = result.length - 1; i >= 0; i--) {
          if (result[i] === null) {
            result.splice(i, 1);
          }
        }

        dispatch({
          type: SELECT_PLACES,
          payload: {
            placesSelected: result,
          },
        });
      })
      .catch((error) => console.log({error}));
  };
}

export async function placeDetails(place) {
  return async (dispatch) => {
    const LATITUDE_DELTA =
      Number(place.geometry.viewport.northeast.lat) -
      Number(place.geometry.viewport.southwest.lat);
    const LONGITUDE_DELTA =
      Number(place.geometry.viewport.northeast.lng) -
      Number(place.geometry.viewport.southwest.lng);

    dispatch({
      type: PLACES_DETAILS,
      payload: {
        showDetails: false,
        region: {
          latitude: place.geometry.location.lat,
          longitude: place.geometry.location.lng,
          latitudeDelta: LATITUDE_DELTA,
          longitudeDelta: LONGITUDE_DELTA,
        },
        place: {
          photos: [],
        },
      },
    });

    if (!place.photos) {
      return dispatch({
        type: PLACES_DETAILS,
        payload: {
          showDetails: true,
          place: {
            ...place,
            photos: [],
          },
        },
      });
    }

    //Extraindo informações desejadas
    let promises = Promise.all(
      place.photos.map(async (photo) => {
        //Realizando request a API da Google Place Photos
        let response = await fetch(
          'https://maps.googleapis.com/maps/api/place/photo?photoreference=' +
            photo.photo_reference +
            '&maxwidth=200&key=AIzaSyBXNwqLXWMzjpOo83hGDPi_rnkLVmBy0WA',
        );

        return {url: response.url};
      }),
    );

    promises
      .then((result) => {
        dispatch({
          type: PLACES_DETAILS,
          payload: {
            showDetails: true,
            place: {
              ...place,
              photos: result,
            },
          },
        });
      })
      .catch((error) => console.log({error}));
  };
}

export function traceRoute() {
  return async (dispatch) => {
    dispatch({type: PLACES_DETAILS, payload: {showDetails: false}});

    dispatch({
      type: TRACE_ROUTE,
      payload: {
        traceRoute: true,
      },
    });
  };
}

export function dismissPlaceDetails() {
  return async (dispatch) => {
    dispatch({
      type: PLACES_DETAILS,
      payload: {
        traceRoute: false,
        showDetails: false,
        place: {
          photos: [],
        },
      },
    });
  };
}

export function dismissTraceRoute() {
  return async (dispatch) => {
    dispatch({
      type: PLACES_DETAILS,
      payload: {traceRoute: false, showDetails: true},
    });
  };
}

export function dismissTravel() {
  return async (dispatch) => {
    let currentPlace = await getCurrentPlace();

    dispatch({
      type: PLACES_DETAILS,
      payload: {
        origin: currentPlace,
        showDetails: false,
        traceRoute: false,
        travelFinish: false,
        travel: {
          id: null,
          status: false,
          location: null,
          speed: null,
          waypoints: [],
        },
      },
    });
  };
}
